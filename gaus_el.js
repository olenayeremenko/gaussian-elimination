/**
 * Created by olenka on 16.05.17.
 */
var N = 0;
var M = 0;

// function resizeMatrix() {
//     var rowsNumber = document.getElementById("rows-number").value;
//     var columnsNumber = document.getElementById("columns-number").value;
//     setMatrixSize(rowsNumber, columnsNumber);
// }

function setMatrixSize(rowsNumber, columnsNumber) {
    N = Number(rowsNumber);
    M = Number(columnsNumber);

    var matrixBlock = document.getElementsByClassName("visualization-block");
    deleteChildren(matrixBlock);
    deleteChildren(document.getElementsByClassName("answers-block"));

    var matrixA = document.createElement('table');
    for (var i = 0; i < N; i++) {
        var tr = document.createElement('tr');
        for (var j = 0; j < N; j++) {
            var td = document.createElement('td');
            var input = document.createElement('input');
            input.type = "text";
            input.id = (i + 1) + "." + (j + 1);
            input.className = "matrixCell";
            input.defaultValue = 0;
            td.appendChild(input);
            tr.appendChild(td);
        }
        matrixA.appendChild(tr);
    }
    matrixA.id = "matrixA";
    matrixBlock.appendChild(matrixA);

    var vectorB = document.createElement('table');
    for (var i = 0; i < N; i++) {
        var tr = document.createElement('tr');
        var td = document.createElement('td');
        var input = document.createElement('input');
        input.type = "text";
        input.id = "b" + (i + 1);
        input.className = "matrixCell";
        input.defaultValue = 0;
        td.appendChild(input);
        tr.appendChild(td);
        vectorB.appendChild(tr);
    }
    vectorB.id = "vectorB";
    matrixBlock.appendChild(vectorB);
}

function readData() {

    var system = new Array(N);
    for (var i = 0; i < N; i++) {
        system[i] = new Array(N + 1);
    }

    for (var i = 0; i < N; i++)
        for (var j = 0; j < N; j++) {
            var s = (i + 1) + "." + (j + 1);
            system[i][j] = document.getElementById(s).value;
        }
    for (var i = 0; i < N; i++) {
        system[i][M - 1] = document.getElementById("b" + (i + 1)).value;
    }

    return system;
}